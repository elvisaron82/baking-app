package br.com.easolutions.bakingapp.ui.activitys;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

import br.com.easolutions.bakingapp.R;
import br.com.easolutions.bakingapp.business.RecipeDetailPresenter;
import br.com.easolutions.bakingapp.data.model.Ingredient;
import br.com.easolutions.bakingapp.data.model.Step;
import br.com.easolutions.bakingapp.interfaces.RecipeDetailContract;
import br.com.easolutions.bakingapp.ui.fragments.ListIngredientsFragment;
import br.com.easolutions.bakingapp.ui.fragments.ListStepFragment;
import br.com.easolutions.bakingapp.utils.ActivityUtils;

import static br.com.easolutions.bakingapp.ui.fragments.ListIngredientsFragment.BUNDLE_LIST_INGREDIENTS_KEY;
import static br.com.easolutions.bakingapp.ui.fragments.ListStepFragment.BUNDLE_LIST_STEPS_KEY;

public class RecipeDetailActivity extends AppCompatActivity implements RecipeDetailContract.View{

    private RecipeDetailPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_detail);
        mPresenter = new RecipeDetailPresenter(this);
        mPresenter.getIntentRecipe(getIntent());
    }

    @Override
    public void displayListOfIngredients(ArrayList<Ingredient> ingredients) {
        ListIngredientsFragment listIngredientsFragment =
                (ListIngredientsFragment) getSupportFragmentManager().findFragmentById(R.id.fl_list_ingredients);

        if (listIngredientsFragment == null) {
            listIngredientsFragment = ListIngredientsFragment.newInstance(ingredients);
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), listIngredientsFragment, R.id.fl_list_ingredients);
        }
    }

    @Override
    public void displayStepList(ArrayList<Step> steps) {
        ListStepFragment listStepFragment =
                (ListStepFragment) getSupportFragmentManager().findFragmentById(R.id.fl_list_steps);

        if (listStepFragment == null) {
            listStepFragment = ListStepFragment.newInstance(steps);
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), listStepFragment, R.id.fl_list_steps);
        }
    }

    @Override
    public void seTitle(String name) {
        setTitle(name);
    }
}
