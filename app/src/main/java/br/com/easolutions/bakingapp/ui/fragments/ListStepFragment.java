package br.com.easolutions.bakingapp.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import br.com.easolutions.bakingapp.R;
import br.com.easolutions.bakingapp.adapters.StepListAdapter;
import br.com.easolutions.bakingapp.data.model.Step;
import br.com.easolutions.bakingapp.interfaces.ListStepContract;
import br.com.easolutions.bakingapp.ui.activitys.StepActivity;

import static br.com.easolutions.bakingapp.interfaces.StepContract.PARCEL_STEP_KEY;


/**
 * ************************************************************
 * Descrição: br.com.easolutions.bakingapp.ui
 * Autor    : Elvis Aron Andrade
 * Data     : 05/04/2018 11:06
 **************************************************************/

public class ListStepFragment extends Fragment implements ListStepContract.View {

    public static final String BUNDLE_LIST_STEPS_KEY = "bundle_list_steps_key";
    private ArrayList<Step> mStepArrayList;

    public static ListStepFragment newInstance(ArrayList<Step> steps) {
        ListStepFragment fragment = new ListStepFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(BUNDLE_LIST_STEPS_KEY, steps);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(BUNDLE_LIST_STEPS_KEY)) {
                mStepArrayList = getArguments().getParcelableArrayList(BUNDLE_LIST_STEPS_KEY);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_list_ingredients, container, false);

        RecyclerView mRecyclerView = layout.findViewById(R.id.rv_ingredient_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        StepListAdapter ingredientListAdapter = new StepListAdapter(mStepArrayList, this);
        mRecyclerView.setAdapter(ingredientListAdapter);
        return layout;
    }

    @Override
    public void onStepItemClick(Step step) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PARCEL_STEP_KEY, step);
        Intent intentStep = new Intent(getContext(), StepActivity.class);
        intentStep.putExtras(bundle);
        startActivity(intentStep);
    }
}
