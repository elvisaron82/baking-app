package br.com.easolutions.bakingapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.easolutions.bakingapp.R;
import br.com.easolutions.bakingapp.data.model.Recipe;
import br.com.easolutions.bakingapp.interfaces.RecipeContract;

import static com.google.common.base.Preconditions.checkNotNull;


/**
 * ************************************************************
 * Descrição: br.com.easolutions.bakingapp
 * Autor    : Elvis Aron Andrade
 * Data     : 03/04/2018 15:25
 **************************************************************/

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.RecipeViewHolder> {

    private List<Recipe> mRecipeList = new ArrayList<>();
    private RecipeContract.View mView;

    public RecipeAdapter(@NonNull RecipeContract.View view) {
        mView = checkNotNull(view, "fragment view cannot be null!");
    }

    @NonNull
    @Override
    public RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.recipe_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutIdForListItem, parent, false);
        return new RecipeAdapter.RecipeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeViewHolder holder, int position) {
        holder.bindData(mRecipeList.get(position));
    }

    @Override
    public int getItemCount() {
        return mRecipeList.size();
    }

    public void setRecipeList(List<Recipe> recipe){
        mRecipeList = recipe;
    }

    public class RecipeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private final TextView mTextViewRecipeTitle;


        public RecipeViewHolder(View itemView) {
            super(itemView);
            mTextViewRecipeTitle = itemView.findViewById(R.id.tv_recipe_title);
            itemView.setOnClickListener(this);
        }

        public void bindData(Recipe recipe) {
            mTextViewRecipeTitle.setText(recipe.getName());
        }

        @Override
        public void onClick(View view) {
            Recipe recipe = mRecipeList.get(getAdapterPosition());
            mView.onRecipeItemClick(recipe);
        }
    }
}
