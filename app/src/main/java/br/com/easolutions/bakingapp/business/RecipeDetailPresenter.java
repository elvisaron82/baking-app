package br.com.easolutions.bakingapp.business;

import android.content.Intent;
import android.support.annotation.NonNull;

import br.com.easolutions.bakingapp.data.model.Recipe;
import br.com.easolutions.bakingapp.interfaces.RecipeDetailContract;

import static br.com.easolutions.bakingapp.interfaces.RecipeDetailContract.PARCEL_RECIPE_KEY;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Elvis Aron Andrade Development Mobile on 05/04/2018 at 06:11.
 * EA Solutions
 * evs.aron@gmail.com
 */
public class RecipeDetailPresenter implements RecipeDetailContract.Presenter {

    private RecipeDetailContract.View mView;
    private Recipe mRecipe;

    public RecipeDetailPresenter(@NonNull RecipeDetailContract.View view){
        mView = checkNotNull(view, "RecipeDetailActivity view cannot be null!");
    }

    @Override
    public void getIntentRecipe(Intent intent) {
        if (intent.hasExtra(PARCEL_RECIPE_KEY)){
            if (intent.getExtras() != null) {
                mRecipe = intent.getExtras().getParcelable(PARCEL_RECIPE_KEY);
                buildScreen();
            }
        }
    }

    private void buildScreen() {
        mView.seTitle(mRecipe.getName());
        mView.displayListOfIngredients(mRecipe.getIngredients());
        mView.displayStepList(mRecipe.getSteps());
    }
}
