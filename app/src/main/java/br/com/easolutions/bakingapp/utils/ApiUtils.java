package br.com.easolutions.bakingapp.utils;

import br.com.easolutions.bakingapp.data.remote.RetrofitClient;
import br.com.easolutions.bakingapp.data.remote.SOService;

/**
 * ************************************************************
 * Descrição: br.com.easolutions.bakingapp.utils
 * Autor    : Elvis Aron Andrade
 * Data     : 03/04/2018 15:15
 **************************************************************/

public class ApiUtils {

    private static final String BASE_URL = "https://d17h27t6h515a5.cloudfront.net/";

    public static SOService getSOService() {
        return RetrofitClient.getClient(BASE_URL).create(SOService.class);
    }
}
