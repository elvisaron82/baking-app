package br.com.easolutions.bakingapp.business;

import android.content.Intent;

import br.com.easolutions.bakingapp.data.model.Step;
import br.com.easolutions.bakingapp.interfaces.StepContract;

import static br.com.easolutions.bakingapp.interfaces.StepContract.PARCEL_STEP_KEY;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Copyright 2018 Rede S.A.
 * ************************************************************
 * Descrição: br.com.easolutions.bakingapp.business
 * Autor    : Elvis Aron Andrade
 * Data     : 05/04/2018 15:26
 * Empresa  : Iteris
 **************************************************************/

public class StepPresenter implements StepContract.Presenter {

    private Step mStep;
    private StepContract.View mView;

    public StepPresenter(StepContract.View view) {
        mView = checkNotNull(view, "StepActivity view cannot be null!");
    }

    @Override
    public void getIntentStep(Intent intent) {
        if (intent.hasExtra(PARCEL_STEP_KEY)){
            if (intent.getExtras() != null) {
                mStep = intent.getExtras().getParcelable(PARCEL_STEP_KEY);
                buildScreen();
            }
        }
    }

    private void buildScreen() {
        mView.buildVideoPlayerFragment(mStep.getVideoURL());
        mView.buildInstructionFragment(mStep.getDescription());
    }
}
