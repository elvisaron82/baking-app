package br.com.easolutions.bakingapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.easolutions.bakingapp.R;
import br.com.easolutions.bakingapp.data.model.Step;
import br.com.easolutions.bakingapp.interfaces.ListStepContract;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * ************************************************************
 * Descrição: br.com.easolutions.bakingapp.adapters
 * Autor    : Elvis Aron Andrade
 * Data     : 05/04/2018 12:26
 **************************************************************/

public class StepListAdapter extends RecyclerView.Adapter<StepListAdapter.StepListViewHolder>{

    private ArrayList<Step> mStepArrayList;
    private ListStepContract.View mView;

    public StepListAdapter(ArrayList<Step> stepArrayList, ListStepContract.View view) {
        mStepArrayList = stepArrayList;
        mView = checkNotNull(view, "fragment ListStep view cannot be null!");
    }
    @NonNull
    @Override
    public StepListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.step_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutIdForListItem, parent, false);
        return new StepListAdapter.StepListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StepListViewHolder holder, int position) {
        holder.bindData(mStepArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return mStepArrayList.size();
    }

    public class StepListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mTextViewShortDescription;

        public StepListViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mTextViewShortDescription = itemView.findViewById(R.id.tv_short_description);
        }

        public void bindData(Step step) {
            String idAndShortDescription = String.valueOf(step.getId()) + "° " + step.getShortDescription();
            mTextViewShortDescription.setText(idAndShortDescription);
        }

        @Override
        public void onClick(View view) {
            Step step = mStepArrayList.get(getAdapterPosition());
            mView.onStepItemClick(step);
        }
    }
}
