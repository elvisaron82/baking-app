package br.com.easolutions.bakingapp.interfaces;

import android.content.Intent;

/**
 * ************************************************************
 * Descrição: br.com.easolutions.bakingapp.interfaces
 * Autor    : Elvis Aron Andrade
 * Data     : 05/04/2018 13:07
 **************************************************************/

public interface StepContract {

    String PARCEL_STEP_KEY = "parcel_step_key";

    interface View {
        void buildInstructionFragment(String description);
        void buildVideoPlayerFragment(String videoURL);
    }

    interface Presenter {
        void getIntentStep(Intent intent);
    }
}
