package br.com.easolutions.bakingapp.data.remote;

import java.util.List;

import br.com.easolutions.bakingapp.data.model.Recipe;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * ************************************************************
 * Descrição: br.com.easolutions.bakingapp.data.remote
 * Autor    : Elvis Aron Andrade
 * Data     : 03/04/2018 14:59
 **************************************************************/

public interface SOService {
    @GET("/topher/2017/May/59121517_baking/baking.json")
    Call<List<Recipe>> getRecipes();
}
