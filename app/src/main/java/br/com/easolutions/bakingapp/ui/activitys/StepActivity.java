package br.com.easolutions.bakingapp.ui.activitys;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v7.app.AppCompatActivity;

import br.com.easolutions.bakingapp.R;
import br.com.easolutions.bakingapp.business.StepPresenter;
import br.com.easolutions.bakingapp.interfaces.StepContract;
import br.com.easolutions.bakingapp.ui.fragments.VideoPlayerFragment;
import br.com.easolutions.bakingapp.utils.ActivityUtils;

public class StepActivity extends AppCompatActivity implements StepContract.View{

    private StepPresenter mPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step);
        mPresenter = new StepPresenter(this);
        mPresenter.getIntentStep(getIntent());
    }

    @Override
    public void buildInstructionFragment(String description) {

    }

    @Override
    public void buildVideoPlayerFragment(String videoURL) {
        VideoPlayerFragment videoPlayerFragment =
                (VideoPlayerFragment) getSupportFragmentManager().findFragmentById(R.id.fl_vide_player);

        if (videoPlayerFragment == null) {
            videoPlayerFragment = VideoPlayerFragment.newInstance(videoURL);
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), videoPlayerFragment, R.id.fl_vide_player);
        }

    }

}
