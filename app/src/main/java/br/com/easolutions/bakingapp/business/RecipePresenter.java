package br.com.easolutions.bakingapp.business;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import br.com.easolutions.bakingapp.data.model.Recipe;
import br.com.easolutions.bakingapp.interfaces.RecipeContract;
import br.com.easolutions.bakingapp.utils.ApiUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * ************************************************************
 * Descrição: br.com.easolutions.bakingapp
 * Autor    : Elvis Aron Andrade
 * Data     : 04/04/2018 11:58
 **************************************************************/

public class RecipePresenter implements RecipeContract.Presenter {

    private RecipeContract.View mView;

    public RecipePresenter(@NonNull RecipeContract.View view){
        mView = checkNotNull(view, "RecipeFragment view cannot be null!");
    }

    @Override
    public void loadRecipes() {
        ApiUtils.getSOService().getRecipes().enqueue(new Callback<List<Recipe>>() {
            @Override
            public void onResponse(Call<List<Recipe>> call, Response<List<Recipe>> response) {

                if(response.isSuccessful()) {
                    mView.displayRecipeList(response.body());
                }else {
                    int statusCode  = response.code();
                    Log.d("RecipeActivity", "else - statusCode: " + statusCode);
                }
            }

            @Override
            public void onFailure(Call<List<Recipe>> call, Throwable t) {
                Log.d("RecipeActivity", "error loading from API");
            }
        });
    }
}
