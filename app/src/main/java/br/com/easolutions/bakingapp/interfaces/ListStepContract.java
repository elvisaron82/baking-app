package br.com.easolutions.bakingapp.interfaces;

import br.com.easolutions.bakingapp.data.model.Step;

/**
 * ************************************************************
 * Descrição: br.com.easolutions.bakingapp.interfaces
 * Autor    : Elvis Aron Andrade
 * Data     : 05/04/2018 13:07
 **************************************************************/

public interface ListStepContract {

    interface View {
        void onStepItemClick(Step step);
    }

}
