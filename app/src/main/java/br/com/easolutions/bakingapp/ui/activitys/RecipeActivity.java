package br.com.easolutions.bakingapp.ui.activitys;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import br.com.easolutions.bakingapp.R;
import br.com.easolutions.bakingapp.ui.fragments.RecipeFragment;
import br.com.easolutions.bakingapp.utils.ActivityUtils;

public class RecipeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecipeFragment mainFragment =
                (RecipeFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);

        if (mainFragment == null) {
            mainFragment = RecipeFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), mainFragment, R.id.contentFrame);
        }
    }
}
