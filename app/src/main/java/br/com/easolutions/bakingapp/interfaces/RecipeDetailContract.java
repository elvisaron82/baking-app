package br.com.easolutions.bakingapp.interfaces;

import android.content.Intent;

import java.util.ArrayList;

import br.com.easolutions.bakingapp.data.model.Ingredient;
import br.com.easolutions.bakingapp.data.model.Step;

/**
 * ************************************************************
 * Descrição: br.com.easolutions.bakingapp
 * Autor    : Elvis Aron Andrade
 * Data     : 04/04/2018 13:55
 **************************************************************/

public interface RecipeDetailContract {

    String PARCEL_RECIPE_KEY = "parcel_recipe_key";

    interface View {
        void displayListOfIngredients(ArrayList<Ingredient> ingredients);
        void displayStepList(ArrayList<Step> steps);
        void seTitle(String name);
    }

    interface Presenter {
        void getIntentRecipe(Intent intent);
    }
}
