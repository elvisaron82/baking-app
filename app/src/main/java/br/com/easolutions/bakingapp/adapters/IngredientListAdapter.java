package br.com.easolutions.bakingapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.easolutions.bakingapp.R;
import br.com.easolutions.bakingapp.data.model.Ingredient;

/**
 * Created by Elvis Aron Andrade Development Mobile on 05/04/2018 at 06:52.
 * EA Solutions
 * evs.aron@gmail.com
 */
public class IngredientListAdapter extends RecyclerView.Adapter<IngredientListAdapter.IngredientListViewHolder> {

    private ArrayList<Ingredient> mIngredientList;

    public IngredientListAdapter(ArrayList<Ingredient> ingredients){
        mIngredientList = ingredients;
    }

    @NonNull
    @Override
    public IngredientListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.ingredient_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutIdForListItem, parent, false);
        return new IngredientListAdapter.IngredientListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull IngredientListViewHolder holder, int position) {
        holder.bindData(mIngredientList.get(position));
    }

    @Override
    public int getItemCount() {
        return mIngredientList.size();
    }

    public class IngredientListViewHolder extends RecyclerView.ViewHolder{

        private final TextView mTextViewQuantity;
        private final TextView mTextViewMeasure;
        private final TextView mTextViewIngrediente;

        public IngredientListViewHolder(View itemView) {
            super(itemView);
            mTextViewQuantity = itemView.findViewById(R.id.tv_quantity);
            mTextViewMeasure = itemView.findViewById(R.id.tv_measure);
            mTextViewIngrediente = itemView.findViewById(R.id.tv_ingredient);
        }

        public void bindData(Ingredient ingredient) {
            mTextViewQuantity.setText(String.valueOf(ingredient.getQuantity()));
            mTextViewMeasure.setText(ingredient.getMeasure());
            mTextViewIngrediente.setText(ingredient.getIngredient());
        }
    }
}
