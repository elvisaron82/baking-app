package br.com.easolutions.bakingapp.ui.fragments;

/**
 * ************************************************************
 * Descrição: br.com.easolutions.bakingapp.ui
 * Autor    : Elvis Aron Andrade
 * Data     : 04/04/2018 10:51
 **************************************************************/

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.easolutions.bakingapp.interfaces.RecipeContract;
import br.com.easolutions.bakingapp.business.RecipePresenter;
import br.com.easolutions.bakingapp.R;
import br.com.easolutions.bakingapp.adapters.RecipeAdapter;
import br.com.easolutions.bakingapp.data.model.Recipe;
import br.com.easolutions.bakingapp.ui.activitys.RecipeDetailActivity;

import static br.com.easolutions.bakingapp.interfaces.RecipeDetailContract.PARCEL_RECIPE_KEY;

public class RecipeFragment extends Fragment implements RecipeContract.View{

    private RecipeAdapter mRecipeAdapter;
    private RecipePresenter mPresenter;

    public static RecipeFragment newInstance() {
        return new RecipeFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRecipeAdapter = new RecipeAdapter(this);
        mPresenter = new RecipePresenter(this);
        mPresenter.loadRecipes();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_main, container, false);

        RecyclerView mRecyclerView = layout.findViewById(R.id.rv_recipe);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mRecipeAdapter);
        return layout;
    }

    @Override
    public void onRecipeItemClick(Recipe recipe) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PARCEL_RECIPE_KEY, recipe);
        Intent intentRecipeDetail = new Intent(getContext(), RecipeDetailActivity.class);
        intentRecipeDetail.putExtras(bundle);
        startActivity(intentRecipeDetail);
    }

    @Override
    public void displayRecipeList(List<Recipe> recipes) {
        mRecipeAdapter.setRecipeList(recipes);
        mRecipeAdapter.notifyDataSetChanged();
    }
}
