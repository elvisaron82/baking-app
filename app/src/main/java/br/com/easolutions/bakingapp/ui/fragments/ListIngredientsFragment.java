package br.com.easolutions.bakingapp.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import br.com.easolutions.bakingapp.R;
import br.com.easolutions.bakingapp.adapters.IngredientListAdapter;
import br.com.easolutions.bakingapp.data.model.Ingredient;

/**
 * ************************************************************
 * Descrição: br.com.easolutions.bakingapp.ui
 * Autor    : Elvis Aron Andrade
 * Data     : 04/04/2018 15:31
 **************************************************************/

public class ListIngredientsFragment extends Fragment {

    public static final String BUNDLE_LIST_INGREDIENTS_KEY = "bundle_list_ingredients_key";
    private ArrayList<Ingredient> mIngredients;

    public static ListIngredientsFragment newInstance(ArrayList<Ingredient> ingredients) {
        ListIngredientsFragment fragment = new ListIngredientsFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(BUNDLE_LIST_INGREDIENTS_KEY, ingredients);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(BUNDLE_LIST_INGREDIENTS_KEY)) {
                mIngredients = getArguments().getParcelableArrayList(BUNDLE_LIST_INGREDIENTS_KEY);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list_ingredients, container, false);

        RecyclerView mRecyclerView = root.findViewById(R.id.rv_ingredient_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        IngredientListAdapter ingredientListAdapter = new IngredientListAdapter(mIngredients);
        mRecyclerView.setAdapter(ingredientListAdapter);
        return root;
    }
}
