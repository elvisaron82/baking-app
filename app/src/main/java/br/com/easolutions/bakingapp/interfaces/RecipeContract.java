package br.com.easolutions.bakingapp.interfaces;

import java.util.List;

import br.com.easolutions.bakingapp.data.model.Recipe;

/**
 * ************************************************************
 * Descrição: br.com.easolutions.bakingapp
 * Autor    : Elvis Aron Andrade
 * Data     : 04/04/2018 11:15
 **************************************************************/

public interface RecipeContract {

    interface View {
        void onRecipeItemClick(Recipe recipe);
        void displayRecipeList(List<Recipe> recipes);
    }

    interface Presenter {
        void loadRecipes();
    }
}
